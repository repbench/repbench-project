# Bandgap Run

Experimental long run with bandgaps, for relaxed structures. 

Identical to the `nmd18` bandgap run, but adding `fl` MBTR. Setup identical to `nmd18` production run.