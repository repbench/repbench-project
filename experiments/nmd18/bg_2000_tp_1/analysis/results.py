from helpers import *
from plotting import plot_curves, plot_pareto_timings
from tabling import print_curves


# generate main results!
# no timings for now.
categories = ["ds_soap_gto", "ds_soap_poly", "m23", "m2", "sf23", "sf2"]


all_curves = {}
all_selected = {}
all_models = {}
for category in categories:
    best, selected = get_best_curve([category + "_a", category + "_b", category + "_c"])

    all_curves[category] = best

    all_selected[category] = selected
    all_models[category] = {
        n: get_model(selected[i], n) for i, n in enumerate(trainsetsizes)
    }


print_curves(all_curves, outdir="results")
plot_curves(all_curves, outdir="results")

print_curves(all_curves, outdir="results", loss="mae")
plot_curves(all_curves, outdir="results", loss="mae")

print_curves(all_curves, outdir="results", loss="rmsle")
plot_curves(all_curves, outdir="results", loss="rmsle")


cmlkit.save_yaml("results/curves.yml", all_curves)
for category, models in all_models.items():
    cmlkit.save_yaml(f"results/models/{category}.yml", models)


# also save a human-readable version of "which restarts were picked?"
nice_selected = {}
for name, selected in all_selected.items():
    stripped = [s.split("_")[-1] for s in selected]
    nice_selected[name] = stripped

cmlkit.save_yaml("results/selected.yml", nice_selected)
