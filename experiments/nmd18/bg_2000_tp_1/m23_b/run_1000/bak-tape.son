run:
  caught_exceptions: [QMMLException, TimeoutError]
  evaluator:
    eval_skrrt:
      data: nmd18-ot-1-1000
      target: bg
      tuner:
        krr_tuner_cv:
          optimizer:
            opt_lgs:
              maxevals: 25
              resolution: null
              rng: 123
  name: '1000'
  search:
    search_hyperopt:
      errors_ok: false
      method: tpe
      seed: 123
      space:
        model:
          per: null
          regression:
            krr:
              kernel:
                kernel_global:
                  kernelf:
                    gaussian:
                      ls: [[hp_quniform, ls_start, -13, 13, 1.0], 1, 0.5, -15, 15]
              nl: [[hp_quniform, nl_start, -18, 0, 1.0], 1, 0.5, -20, 2]
          representation: [[hp_choice, mbtr_2_type, [{mbtr_2: {acc: 0.001, aindexf: noreversals,
                    broadening: [hp_loggrid, mbtr2_dist_broadening, -20, 3, 47], eindexf: noreversals,
                    elems: [8, 13, 31, 49], geomf: 1/distance, norm: [hp_choice, mbtr2_dist_norm,
                      [{none: {}}, {simple: {scale: [hp_loggrid, mbtr2_dist_norm_val,
                              -10, 10, 21]}}]], num: 100, start: -0.04, stop: 0.83,
                    weightf: [hp_choice, mbtr2_dist_weightf, [identity^2, {exp_-1/identity: {
                            ls: [hp_loggrid, mbtr_dist_2_wf_ls_1, -20, 0, 21]}}, {
                          exp_-1/identity^2: {ls: [hp_loggrid, mbtr_dist_2_wf_ls_2,
                              -20, 0, 21]}}]]}}, {mbtr_2: {acc: 0.001, aindexf: noreversals,
                    broadening: [hp_loggrid, mbtr2_dot_broadening, -20, 3, 47], eindexf: noreversals,
                    elems: [8, 13, 31, 49], geomf: 1/dot, norm: [hp_choice, mbtr2_dot_norm,
                      [{none: {}}, {simple: {scale: [hp_loggrid, mbtr2_dot_norm_val,
                              -10, 10, 21]}}]], num: 100, start: -0.03, stop: 0.63,
                    weightf: [hp_choice, mbtr2_dot_weightf, [identity^2, {exp_-1/identity: {
                            ls: [hp_loggrid, mbtr_dot_2_wf_ls_1, -20, 0, 21]}}, {
                          exp_-1/identity^2: {ls: [hp_loggrid, mbtr_dot_2_wf_ls_2,
                              -20, 0, 21]}}]]}}]], [hp_choice, mbtr_3_type, [{mbtr_3: {
                    acc: 0.01, aindexf: noreversals, broadening: [hp_loggrid, mbtr3_ang_broadening,
                      -20, 4, 49], eindexf: noreversals, elems: [8, 13, 31, 49], geomf: angle,
                    norm: [hp_choice, mbtr3_ang_norm, [{none: {}}, {simple: {scale: [
                              hp_loggrid, mbtr3_ang_norm_val, -10, 10, 21]}}]], num: 100,
                    start: -0.16, stop: 3.46, weightf: [hp_choice, mbtr3_ang_weightf,
                      [1/dotdotdot, {exp_-1/normnormnorm: {ls: [hp_loggrid, mbtr3_ang_wf_ls_1,
                              -20, 0, 21]}}, {exp_-1/norm+norm+norm: {ls: [hp_loggrid,
                              mbtr3_ang_wf_ls_2, -20, 0, 21]}}]]}}, {mbtr_3: {acc: 0.01,
                    aindexf: noreversals, broadening: [hp_loggrid, mbtr3_cos_broadening,
                      -20, 4, 49], eindexf: noreversals, elems: [8, 13, 31, 49], geomf: cos_angle,
                    norm: [hp_choice, mbtr3_cos_norm, [{none: {}}, {simple: {scale: [
                              hp_loggrid, mbtr3_cos_norm_val, -10, 10, 21]}}]], num: 100,
                    start: -1.05, stop: 1.05, weightf: [hp_choice, mbtr3_cos_weightf,
                      [1/dotdotdot, {exp_-1/normnormnorm: {ls: [hp_loggrid, mbtr3_cos_wf_ls_1,
                              -20, 0, 21]}}, {exp_-1/norm+norm+norm: {ls: [hp_loggrid,
                              mbtr3_cos_wf_ls_2, -20, 0, 21]}}]]}}, {mbtr_3: {acc: 0.01,
                    aindexf: noreversals, broadening: [hp_loggrid, mbtr3_ddd_broadening,
                      -20, 3, 47], eindexf: noreversals, elems: [8, 13, 31, 49], geomf: dot/dotdot,
                    norm: [hp_choice, mbtr3_ddd_norm, [{none: {}}, {simple: {scale: [
                              hp_loggrid, mbtr3_ddd_norm_val, -10, 10, 21]}}]], num: 100,
                    start: -0.03, stop: 0.63, weightf: [hp_choice, mbtr3_ddd_weightf,
                      [1/dotdotdot, {exp_-1/normnormnorm: {ls: [hp_loggrid, mbtr3_ddd_wf_ls_1,
                              -20, 0, 21]}}, {exp_-1/norm+norm+norm: {ls: [hp_loggrid,
                              mbtr3_ddd_wf_ls_2, -20, 0, 21]}}]]}}]]]
  stop:
    stop_max:
      count: 2000
      errors: true
      use: trials
  trial_timeout: 10000.0

===
