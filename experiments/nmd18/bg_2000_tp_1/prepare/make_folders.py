import pathlib
import shutil

for prefix in ["sf2", "sf23", "m2", "m23", "ds_soap_poly", "ds_soap_gto"]:
    for repeat in ["_a", "_b", "_c"]:
        name = prefix
        # print(name)
        folder = pathlib.Path("../" + name + repeat)
        folder.mkdir(exist_ok=True)
        shutil.copy("meta_" + prefix + ".yml", folder / "meta.yml")
        shutil.copy("space_" + prefix + ".yml", folder / "space.yml")
