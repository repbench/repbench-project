import pathlib
import shutil

for prefix in ["sf2", "sf23", "m2", "m23", "ds_soap_poly", "ds_soap_gto"]:
    (pathlib.Path("meta_" + prefix + ".yml")).touch()
    (pathlib.Path("space_" + prefix + ".yml")).touch()
