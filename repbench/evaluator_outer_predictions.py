"""Evaluator that returns a lot of diagnostic information to troubleshoot precictions."""

import numpy as np

from importlib import import_module
from pebble import ProcessPool

from cmlkit import load_dataset, from_config
from cmlkit.evaluation.evaluator import Evaluator


class EvaluatorOuterPredictions(Evaluator):
    """Generates predictions for points on the learning curve.

    (Rather than losses.) (We default to computing only losses
    to avoid saving/comitting large amounts of unused data.)

    Evaluates one model on the 10 outer train/validation
    splits for one particular training set size n, returns
    a dict with losses as keys, and values further dicts containing
        - "train": predictions on training set (converted to target per)
        - "test": predictions on test set

    """

    kind = "eval_outer_predictions"

    default_context = {"max_workers": 2}

    def __init__(self, n, dataset, context={}):
        super().__init__(context=context)

        self.n = n
        self.dataset = dataset

        if dataset == "gdb9":
            self.target = "u0"
            self.per = "mol"
        elif dataset == "ba10":
            self.target = "fe"
            self.per = "atom"
        else:
            raise ValueError(
                f"Repbench is restricted to datasets gdb9 and ba10, not {dataset}."
            )

    def _get_config(self):
        return {"n": self.n, "dataset": self.dataset}

    def evaluate(self, model):
        data = import_module(f"data.{self.dataset}")

        evaluation = [
            {
                "train": data.name_ot(i, self.n),
                "test": data.name_ov(i),
                "target": self.target,
                "per": self.per,
            }
            for i in range(1, data.numov + 1)
        ]

        with ProcessPool(max_workers=self.context["max_workers"]) as p:
            tasks = [(model, e) for e in evaluation]
            raw_results = p.map(evaluate, tasks)
            raw_results = list(raw_results.result())

        pred_train = np.array(
            [raw_results[i - 1]["pred_train"] for i in range(1, data.numov + 1)]
        )
        pred_test = np.array(
            [raw_results[i - 1]["pred_test"] for i in range(1, data.numov + 1)]
        )

        return {"pred_train": pred_train, "pred_test": pred_test}


def evaluate(task):
    model, evaluation = task

    train = load_dataset(evaluation["train"])
    test = load_dataset(evaluation["test"])

    model.train(train, target=evaluation["target"])
    pred_test = model.predict(test, per=evaluation["per"])
    pred_train = model.predict(train, per=evaluation["per"])

    return {"pred_train": pred_train, "pred_test": pred_test}
