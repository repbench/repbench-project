"""Evaluator using the NOMAD2018 challenge splits, losses and targets."""

import numpy as np

from importlib import import_module
from pebble import ProcessPool

from cmlkit import load_dataset, from_config
from cmlkit.evaluation.evaluator import Evaluator
from cmlkit.evaluation.evaluator_holdout import EvaluatorHoldout


class EvaluatorNOMAD(Evaluator):
    """Re-creates the NOMAD2018 Kaggle challenge.

    Evaluates one model on the Kaggle challenge splits,
    both with and without duplicates, for either bandgap
    or formation energy.

    """

    kind = "eval_nomad"

    default_context = {"max_workers": 2}

    def __init__(self, target="fe", context={}):
        super().__init__(context=context)

        self.loss = ["rmsle", "rmse", "mae", "r2", "cod", "medianae", "maxae"]

        if target == "bg":
            self.per = None
        elif target == "fe":
            self.per = "non_O"
        else:
            raise ValueError(
                f"Repbench NOMAD2018 evaluator is not set up to predict {target}."
            )

        self.target = target

    def _get_config(self):
        return {"target": self.target}

    def evaluate(self, model):
        evaluators = [
            {
                "eval_holdout": {
                    "train": "nmd18_train",
                    "test":  "nmd18_test",
                    "target": self.target,
                    "per": self.per,
                    "loss": self.loss,
                }
            },
            {
                "eval_holdout": {
                    "train": "nmd18_train-nodup",
                    "test":  "nmd18_test-nodup",
                    "target": self.target,
                    "per": self.per,
                    "loss": self.loss,
                }
            }
        ]

        with ProcessPool(max_workers=self.context["max_workers"]) as p:
            tasks = [(model, e) for e in evaluators]
            results = p.map(evaluate, tasks)
            results = list(results.result())

        return {"full": results[0], "nodup": results[1]}


def evaluate(a):
    model, evaluator = a
    evaluator = from_config(evaluator)

    return evaluator(model)
