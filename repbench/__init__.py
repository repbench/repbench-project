from .evaluator_outer import EvaluatorOuter
from .evaluator_outer_predictions import EvaluatorOuterPredictions
from .evaluator_nomad import EvaluatorNOMAD

components = [EvaluatorOuter, EvaluatorOuterPredictions, EvaluatorNOMAD]
