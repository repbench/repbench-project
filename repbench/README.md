## Repbench cmlkit plugin 🧩

This provides some `Components` that are used within the `repbench` project. This therefore needs to be declared as `cmlkit` plugin by adding it to the list of plugins. The repbench parent folder should already be in the PYTHONPATH. The particular `Components` provided are `Evaluators` 