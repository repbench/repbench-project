"""Evaluator that generates one (set) of points on the learning curve."""

import numpy as np

from importlib import import_module
from pebble import ProcessPool

from cmlkit import load_dataset, from_config
from cmlkit.evaluation.evaluator import Evaluator
from cmlkit.evaluation.evaluator_holdout import EvaluatorHoldout


class EvaluatorOuter(Evaluator):
    """Generates one point on the learning curve.

    Evaluates one model on the 10 outer train/validation
    splits for one particular training set size n, returns
    a dict of form:
        "lossname": {"mean": 1, "std": 2, "all": [123123]}
    """

    kind = "eval_outer"

    default_context = {"max_workers": 2}

    def __init__(self, n, dataset, target=None, context={}):
        super().__init__(context=context)

        self.n = n
        self.dataset = dataset

        self.loss = ["rmse", "mae", "r2", "medianae", "maxae"]

        if dataset == "gdb9":
            self.target = "u0"
            self.per = "mol"
        elif dataset == "ba10":
            self.target = "fe"
            self.per = "atom"
        elif dataset == "nmd18" or dataset == "xnmd18":
            self.target = "fe"
            self.per = "non_O"
            self.loss.append("rmsle")
        else:
            raise ValueError(
                f"Repbench is restricted to datasets gdb9 and ba10 and (x)nmd18, not {dataset}."
            )

        if target is not None:
            if target == "bg" and (dataset == "nmd18" or dataset == "xnmd18"):
                self.target = target
                self.per = None
            else:
                raise ValueError(
                    f"Repbench is not set up to predict {target} for dataset {dataset}."
                )

    def _get_config(self):
        return {"n": self.n, "dataset": self.dataset, "target": self.target}

    def evaluate(self, model):
        data = import_module(f"data.{self.dataset}")

        evaluators = [
            {
                "eval_holdout": {
                    "train": data.name_ot(i, self.n),
                    "test": data.name_ov(i),
                    "target": self.target,
                    "per": self.per,
                    "loss": self.loss,
                }
            }
            for i in range(1, data.numov + 1)
        ]

        with ProcessPool(max_workers=self.context["max_workers"]) as p:
            tasks = [(model, e) for e in evaluators]
            raw_results = p.map(evaluate, tasks)
            raw_results = list(raw_results.result())

        results = {}
        # collect losses for easy post-processing
        for l in self.loss:
            collected = np.array(
                [raw_results[i - 1][l] for i in range(1, data.numov + 1)]
            )
            mean = np.mean(collected)
            std = np.std(collected)

            results[l] = {"mean": mean, "std": std, "all": collected.tolist()}

        return results


def evaluate(a):
    model, evaluator = a
    evaluator = from_config(evaluator)

    return evaluator(model)
