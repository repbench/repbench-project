from .main import falco, click

from pathlib import Path
from cmlkit import from_config
from cmlkit.engine.inout import save_yaml, read_yaml, save_npy
import subprocess

from jobro.generate import generate_jobscript


@falco.group()
@click.pass_context
def post(ctx):
    pass


@post.command()
@click.pass_context
@click.argument("curve", type=click.Path())
@click.argument("n", default="all")
@click.option("--target", default=None)
def curve(ctx, curve, n, target=None):
    curve = Path(curve)
    meta = read_yaml(curve / "meta")["curve"]

    if n == "all":
        n = ""

    if target is None:
        target_string = ""
    else:
        target_string = f" --target={target}"

    generate_jobscript(
        duration=meta["duration"],
        file=f"curve_{curve}.sh",
        command=f"falco post runcurve{target_string} {curve} {n}",
        name=str(curve) + "_curve",
    )

    subprocess.run(["sbatch", f"curve_{curve}.sh"], cwd=Path("."))


@post.command()
@click.pass_context
@click.argument("curve", type=click.Path())
@click.argument("n", default="all")
@click.option("--target", default=None)
def runcurve(ctx, curve, n, target=None):
    shared = ctx.obj["shared"]
    curve = Path(curve)
    (curve / "post").mkdir(exist_ok=True)

    meta = read_yaml(curve / "meta")["curve"]

    if n == "all":
        trainsetsizes = shared.trainsetsizes
        models = shared.get_models(curve)
    else:
        trainsetsizes = [int(n)]
        models = [shared.get_model(curve, int(n))]

    results = {}
    for i, n in enumerate(trainsetsizes):
        evaluator = {"eval_outer": {"n": n, "dataset": shared.dataset, "target": target}}
        results[n] = from_config(evaluator, context=meta)(models[i])
        save_yaml(curve / "post/curve.yml", results)
        save_yaml(curve / f"post/curve_model-{trainsetsizes[i]}", models[i])
