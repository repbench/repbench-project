"""One-off/bonus tasks."""

from .post import *


@post.group()
@click.pass_context
def bonus(ctx):
    pass


@bonus.command()
@click.pass_context
@click.argument("curve", type=click.Path())
def predictions(ctx, curve):
    """Generate predictions (not losses)"""

    curve = Path(curve)
    meta = read_yaml(curve / "meta")["curve"]

    generate_jobscript(
        duration=2
        * meta[
            "duration"
        ],  # we are doing about twice as much as the regular curve generation ;)
        file=f"pred_{curve}.sh",
        command=f"falco post bonus predictions-run {curve}",
        name=str(curve) + "_pred",
    )

    subprocess.run(["sbatch", f"pred_{curve}.sh"], cwd=Path("."))


@bonus.command()
@click.pass_context
@click.argument("curve", type=click.Path())
def predictions_run(ctx, curve):
    shared = ctx.obj["shared"]
    curve = Path(curve)
    (curve / "post/predictions").mkdir(exist_ok=True)

    meta = read_yaml(curve / "meta")["curve"]

    models = shared.get_models(curve)

    results = {}
    for i, n in enumerate(shared.trainsetsizes):
        print(f"Working on n={n}.")
        evaluator = {"eval_outer_predictions": {"n": n, "dataset": shared.dataset}}
        results = from_config(evaluator, context=meta)(models[i])
        save_npy(curve / f"post/predictions/{shared.trainsetsizes[i]}", results)
        save_yaml(curve / f"post/predictions/model-{shared.trainsetsizes[i]}", models[i])


@bonus.command()
@click.pass_context
@click.argument("curve", type=click.Path())
@click.argument("n", default="all")
@click.option("--target", default="fe")
def nomad(ctx, curve, n, target="fe"):
    curve = Path(curve)
    meta = read_yaml(curve / "meta")["curve"]

    if n == "all":
        n = ""

    generate_jobscript(
        duration=2 * meta["duration"],
        file=f"nomad_{curve}.sh",
        command=f"falco post bonus nomad-run --target={target} {curve} {n}",
        name=str(curve) + "_nomad",
    )

    subprocess.run(["sbatch", f"nomad_{curve}.sh"], cwd=Path("."))


@bonus.command()
@click.pass_context
@click.argument("curve", type=click.Path())
@click.argument("n", default="all")
@click.option("--target", default="fe")
def nomad_run(ctx, curve, n, target="fe"):
    shared = ctx.obj["shared"]
    curve = Path(curve)
    (curve / "post/nomad").mkdir(exist_ok=True, parents=True)

    meta = read_yaml(curve / "meta")["curve"]

    if n == "all":
        trainsetsizes = shared.trainsetsizes
        models = shared.get_models(curve)
    else:
        trainsetsizes = [int(n)]
        models = [shared.get_model(curve, int(n))]

    results = {}
    for i, n in enumerate(trainsetsizes):
        evaluator = {"eval_nomad": {"target": target}}
        results[n] = from_config(evaluator, context=meta)(models[i])
        save_yaml(curve / "post/nomad/results.yml", results)
        save_yaml(curve / f"post/nomad/model-{trainsetsizes[i]}", models[i])


@bonus.command()
@click.pass_context
@click.argument("curve", type=click.Path())
def curve_all_models(ctx, curve):
    """Use the models tuned for each size to predict each size.

    ... as opposed to using a separate model for each point on the curve.

    Used for double-checking whether per-size optimisation is preferable.

    """
    curve = Path(curve)
    meta = read_yaml(curve / "meta")["curve"]

    generate_jobscript(
        duration=meta["duration"] * len(ctx.obj["shared"].trainsetsizes),
        file=f"curve_all_{curve}.sh",
        command=f"falco post bonus curve-all-models-run {curve}",
        name=str(curve) + "_allcurve",
    )

    subprocess.run(["sbatch", f"curve_all_{curve}.sh"], cwd=Path("."))


@bonus.command()
@click.pass_context
@click.argument("curve", type=click.Path())
def curve_all_models_run(ctx, curve):
    shared = ctx.obj["shared"]
    curve = Path(curve)
    (curve / "post/curves_for_all_models").mkdir(exist_ok=True, parents=True)

    meta = read_yaml(curve / "meta")["curve"]

    trainsetsizes = shared.trainsetsizes
    models = shared.get_models(curve)

    for j, m in enumerate(models):
        results = {}
        for i, n in enumerate(trainsetsizes):
            evaluator = {"eval_outer": {"n": n, "dataset": shared.dataset}}
            results[n] = from_config(evaluator, context=meta)(m)

            save_yaml(
                curve / f"post/curves_for_all_models/{trainsetsizes[j]}.yml", results
            )
