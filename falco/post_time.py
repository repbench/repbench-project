"""The 'new' timing infrastructure. Preferred over the timing/kerneltiming commands."""

from .post import *


@post.group()
@click.pass_context
def time(ctx):
    pass


@time.command()
@click.pass_context
@click.argument("curve", type=click.Path())
@click.option("--queue", default=None)
@click.option("--duration", default=None)
def rep(ctx, curve, queue=None, duration=None):
    curve = Path(curve)
    meta = read_yaml(curve / "meta")["timing"]

    if duration is None:
        duration = meta["duration"]
    else:
        duration = int(duration)

    generate_jobscript(
        duration=duration,
        file=f"time_rep_{curve}.sh",
        command=f"falco post time rep-run {curve}",
        name=str(curve) + "_time_rep",
        queue=queue,
    )

    subprocess.run(["sbatch", f"time_rep_{curve}.sh"], cwd=Path("."))


@time.command()
@click.pass_context
@click.argument("curve", type=click.Path())
def rep_run(ctx, curve):
    shared = ctx.obj["shared"]
    curve = Path(curve)

    evaluator = from_config({"eval_reptimer": {"data": shared.data.name_ov(1)}})

    outdir = (
        curve
        / f"post/time/rep/{evaluator.cpu_info['family']}-{evaluator.cpu_info['model']}"
    )
    outdir.mkdir(exist_ok=True, parents=True)

    meta = read_yaml(curve / "meta")["curve"]

    models = shared.get_models(curve)

    save_yaml(outdir / "cpu.yml", evaluator.cpu_info)

    results = {
        shared.trainsetsizes[i]: evaluator(model)["timings"]
        for i, model in enumerate(models)
    }

    save_yaml(outdir / "times.yml", results)
    for i, m in enumerate(models):
        save_yaml(outdir / f"model-{shared.trainsetsizes[i]}", m)


@time.command()
@click.pass_context
@click.argument("curve", type=click.Path())
@click.option("--queue", default=None)
@click.option("--duration", default=None)
def kernel(ctx, curve, queue=None, duration=None):
    curve = Path(curve)
    meta = read_yaml(curve / "meta")["timing"]

    if duration is None:
        duration = 3 * meta["duration"]
    else:
        duration = int(duration)

    generate_jobscript(
        duration=duration,
        file=f"time_kernel_{curve}.sh",
        command=f"falco post time kernel-run {curve}",
        name=str(curve) + "_time_kernel",
        queue=queue,
    )

    subprocess.run(["sbatch", f"time_kernel_{curve}.sh"], cwd=Path("."))


@time.command()
@click.pass_context
@click.argument("curve", type=click.Path())
def kernel_run(ctx, curve):
    shared = ctx.obj["shared"]
    curve = Path(curve)

    evaluator = from_config({"eval_kerneltimer": {"data": shared.data.name_ov(1)}})

    outdir = (
        curve
        / f"post/time/kernel/{evaluator.cpu_info['family']}-{evaluator.cpu_info['model']}"
    )
    outdir.mkdir(exist_ok=True, parents=True)

    meta = read_yaml(curve / "meta")["curve"]

    models = shared.get_models(curve)

    save_yaml(outdir / "cpu.yml", evaluator.cpu_info)

    results = {
        shared.trainsetsizes[i]: evaluator(model)["timings"]
        for i, model in enumerate(models)
    }

    save_yaml(outdir / "times.yml", results)
    for i, m in enumerate(models):
        save_yaml(outdir / f"model-{shared.trainsetsizes[i]}", m)
