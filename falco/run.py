from .main import falco, click

from pathlib import Path
from jobro.generate import generate_jobscript
import subprocess

from cmlkit.engine.inout import read_yaml


@falco.command()
@click.pass_context
@click.argument("curve", type=click.Path())
@click.argument("n", default="all")
@click.option("--duration", default=0)
def run(ctx, curve, n, duration):
    trainsetsizes = ctx.obj["shared"].trainsetsizes

    if n == "all":
        for n in trainsetsizes:
            _run(curve, n, duration)
    else:
        n = int(n)
        _run(curve, n, duration)


def _run(curve, n, duration):
    curve = Path(curve)
    meta = read_yaml(curve / "meta.yml")
    if duration == 0:
        duration = meta[n]["duration"]

    generate_jobscript(
        duration=duration,
        file=curve / f"sub_{n}.sh",
        command=f"falcont {n} {duration}",
        name=str(curve) + f"_{n}",
    )

    subprocess.run(["sbatch", f"sub_{n}.sh"], cwd=curve)
