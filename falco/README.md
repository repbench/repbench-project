## `falco`: fully automated learning curves with optimisation

This is just a little `click` application to automate various processes for this project.

`falco` has the following sub commands:

- `prepare`: generate `cmlkit.tune.Run` instances for a given curveset and model class
- `run`: submit to `slurm`
- `post/post_time/post_bonus`: make predictions, run timings, various associated post-processing tasks

The `falcont` command is used to "continue" existing runs. (Aside: This is needed because this infrastructure does not support automatic slurm resubmission and expects to run continuously in one job. Nowadays, I'd use something similar to the [`vibes`](https://gitlab.com/vibes-developers/vibes/) slurm interface that automatically resubmits unfinished jobs.)

The only special thing about `falco` is that it expects to be run inside a `repbench` curveset directory, which contains a `shared/` subfolder that is importable (i.e. contains an `__init__.py`). This module is passed to all commands as `ctx.object["shared"]`. (Aside: This is a hack, but it works. Nowadays, I would wrap this more cleanly.)