import click

import pathlib
from importlib import import_module
import sys


@click.group()
@click.pass_context
def falco(ctx):
    ctx.ensure_object(dict)
    cwd = pathlib.Path.cwd()
    ctx.obj["cwd"] = cwd
    sys.path.append(str(cwd))
    ctx.obj["shared"] = import_module("shared")


@falco.command()
@click.pass_context
def test(ctx):
    print(ctx.obj["shared"].trainsetsizes)
