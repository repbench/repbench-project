from pathlib import Path

from .main import falco, click

@falco.command()
@click.argument("curve", type=click.Path())
@click.pass_context
def prepare(ctx, curve):
    path = Path(curve)
    shared = ctx.obj["shared"]

    for n in shared.trainsetsizes:
        run = shared.make_run(path / "space", n)
        run.prepare(path)
