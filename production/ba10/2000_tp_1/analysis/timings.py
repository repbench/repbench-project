from helpers import *
from plotting import plot_curves, plot_combo_pareto_timings, plot_pareto_timings, plot_ultra_pareto_timings
from tabling import print_curves

# Plot results!
categories = [
    "m2_pa",
    "m2fl_pa",
    "m23_pa",
    "sf2_ps",
    "sf23_ps",
    "ds_soap_gto_ps",
    "ds_soap_poly_ps",
    "soap_ps",
    # "ds_soap_poly_pa",
    # "m2_ps",
    # "m2nn_ps",
    # "m2nn_pa",
    # "m23_ps",
    # "m23nn_ps",
    # "m23nn_pa",
    # "m23nn_ps",
    # "m23nn_pa",
    # "m23mini_ps",
    # "m23mini_pa",
    # "m23fn_ps",
    # "m23fn_pa",
]

all_curves = {}
all_selected = {}
all_rep_timings = {}
all_kernel_timings = {}
all_models = {}
for category in categories:
    best, selected = get_best_curve([category + "_a", category + "_b", category + "_c"])

    all_curves[category] = best
    all_selected[category] = selected

    rep_timings, _ = get_best_timings(
        [category + "_a", category + "_b", category + "_c"], selected=selected, kind="rep"
    )
    all_rep_timings[category] = rep_timings

    kernel_timings, _ = get_best_timings(
        [category + "_a", category + "_b", category + "_c"], selected=selected, kind="kernel"
    )
    all_kernel_timings[category] = kernel_timings

    all_models[category] = {
        n: get_model(selected[i], n) for i, n in enumerate(trainsetsizes)
    }

# print_curves(all_curves, outdir="results")
# plot_curves(all_curves, outdir="results")
plot_combo_pareto_timings(all_rep_timings, all_kernel_timings, all_curves, outdir="timings")
# plot_pareto_timings(all_kernel_timings, all_curves, outdir="timings", kind="kernel")
plot_ultra_pareto_timings(all_rep_timings, all_kernel_timings, all_curves, outdir="timings")


print("Which were selected?")
for name, selected in all_selected.items():
    stripped = [s.split("_")[-1] for s in selected]
    print(f"{name}: {stripped}")
