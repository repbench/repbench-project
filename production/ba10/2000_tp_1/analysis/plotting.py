from helpers import *

import matplotlib.ticker as ticker
from scipy.optimize import curve_fit
import seaborn as sns
import matplotlib

matplotlib.use("tkagg")  # seems to be needed to make matplotlib work on macOS
plt = matplotlib.pyplot

matplotlib.rcParams.update({"font.size": 15})

from itertools import cycle

lines = ["-", "--", "-.", ":"]
linecycler = cycle(lines)

names = {
    "sf2_ps": "SF $k=2$ (ps)",
    "sf23_ps": "SF $k=2,3$ (ps)",
    "sf2_pa": "SF $k=2$ (pa)",
    "sf23_pa": "SF $k=2, 3$ (pa)",
    "soap_ps": "SOAP",
    "ds_soap_gto_ps": "SOAP (dscribe, gto basis)",
    "ds_soap_poly_ps": "SOAP (dscribe, poly basis)",
    "m2_ps": "MBTR $k=2$ (ps)",
    "m23_ps": "MBTR $k=2,3$ (ps)",
    "m2_pa": "MBTR $k=2$ (pa)",
    "m2fl_pa": "MBTR $k=2$ (pa, full index)",
    "m23fl_pa": "MBTR $k=2,3$ (pa, full index)",
    "m23_pa": "MBTR $k=2,3$ (pa)",
}


def fig_and_ax(figsize=None):
    if figsize:
        fig = plt.figure(figsize=figsize, dpi=200)
    else:
        fig = plt.figure(figsize=(8, 8), dpi=200)
    ax = plt.axes()
    return fig, ax


def fit_func(x, d, c):
    return -d * x + c


def fit_curve(x, y):
    lx = np.log10(x)
    ly = np.log10(y)

    popt, pcov = curve_fit(fit_func, lx, ly, p0=[-2.0, 50.0])

    return 10 ** fit_func(lx, *popt)


def formatter(y, pos):
    return np.round(y, 3)


####


def plot_curves(curves, loss="rmse", prefix="lc", fit=False, outdir=Path()):
    """Plot learning curves

    Args:
        curves: dict with keys corresponding to names of curve,
            and values a standard learning curve dict
        loss: loss to be used
        prefix: filename will be prefix_lossname.pdf
        fit: if True, plot linear fit, else just connect the dots
        outdir: where to save plots

    """
    outdir = Path(outdir)
    fig, ax = fig_and_ax(figsize=(9, 6))

    ax.set_xlabel("Training set size")
    ax.set_ylabel(f"{loss.upper()} of $E_f$ in meV/atom")

    ax.set_xscale("log", basex=10)
    ax.set_yscale("log", basey=10, subsy=[2, 2.5, 3, 4, 5, 6, 8, 12, 14, 16, 18])

    ax.set_xticks(trainsetsizes)
    ax.set_xticklabels(trainsetsizes)

    ax.yaxis.set_major_formatter(ticker.FuncFormatter(formatter))
    ax.yaxis.set_minor_formatter(ticker.FuncFormatter(formatter))

    colors = iter(sns.color_palette("bright", len(curves)))

    for name, curve in curves.items():
        color = next(colors)
        linestyle = next(linecycler)
        n = list(curve.keys())

        if fit:
            lw = 0
        else:
            lw = 1

        mean = [result[loss]["mean"] * 1000 for result in curve.values()]
        alll = np.array([result[loss]["all"] for result in curve.values()])

        ax.plot(
            n,
            mean,
            linestyle=linestyle,
            marker="x",
            label=names.get(name, name),
            color=color,
            lw=lw,
            markersize=10,
        )
        if fit:
            fit = fit_curve(n, mean)
            ax.plot(n, fit, color=color, alpha=1.0)

        for i in range(10):
            if i == 0:
                ax.plot(n, alll[:, i] * 1000, marker="+", color=color, lw=0, alpha=0.2)
            else:
                ax.plot(n, alll[:, i] * 1000, marker=".", color=color, lw=0, alpha=0.2)

    handles, labels = plt.gca().get_legend_handles_labels()
    labels, ids = np.unique(labels, return_index=True)
    handles = [handles[i] for i in ids]
    plt.legend(handles, labels, loc="upper right")

    fig.savefig(outdir / f"{prefix}_{loss}.pdf", bbox_inches="tight")


def plot_combo_pareto_timings(
    rep_timings,
    kernel_timings,
    curves,
    loss="rmse",
    prefix="t",
    ns=trainsetsizes,
    outdir=Path(),
):
    outdir = Path(outdir)

    for n in ns:
        # print(f"n={n}\n")
        colors = iter(sns.color_palette("bright", len(rep_timings)))
        fig, ax = fig_and_ax(figsize=(9, 6))
        ax.set_xlabel("Time in ms")
        ax.set_ylabel(f"{loss.upper()} of $E_f$ in meV/atom")

        handles = []

        for name in rep_timings.keys():

            c = next(colors)
            tr = 1000 * rep_timings[name][n]["run"]["mean"] / 1000.0
            tk = 1000 * kernel_timings[name][n]["mean"] / 1000.0
            l = curves[name][n][loss]["mean"]
            ax.scatter(tk + tr, l, color=c, marker="o", label=names.get(name, name))
            ax.scatter(tr, l, color=c, marker="$R$", label=names.get(name, name))
            ax.scatter(tk, l, color=c, marker="$K$", label=names.get(name, name))

            # handles.append((p1, p2, p3))

            # print(f"{name}: {t:.2f} @ {l:.2f}")

        handles, labels = plt.gca().get_legend_handles_labels()
        labels, ids = np.unique(labels, return_index=True)
        handles = [handles[i] for i in ids]
        plt.legend(handles, labels, loc="upper right")

        fig.savefig(outdir / f"{prefix}_{loss}_{n}.pdf", bbox_inches="tight")


def plot_pareto_timings(
    timings, curves, loss="rmse", prefix="t", ns=trainsetsizes, outdir=Path(), kind="rep"
):
    outdir = Path(outdir)

    for n in ns:
        # print(f"n={n}\n")
        colors = iter(sns.color_palette("bright", len(timings)))
        fig, ax = fig_and_ax(figsize=(9, 6))
        ax.set_xlabel(f"Time to compute {kind} in ms")
        ax.set_ylabel(f"{loss.upper()} of $E_f$ in meV/atom")

        for name, timing in timings.items():
            c = next(colors)
            if kind == "rep":
                t = 1000 * timing[n]["run"]["mean"] / 1000.0
            elif kind == "kernel":
                t = 1000 * timing[n]["mean"] / 1000.0

            l = curves[name][n][loss]["mean"]
            ax.scatter(t, l, color=c, marker="o", label=names.get(name, name))

            # print(f"{name}: {t:.2f} @ {l:.2f}")

        handles, labels = plt.gca().get_legend_handles_labels()
        labels, ids = np.unique(labels, return_index=True)
        handles = [handles[i] for i in ids]
        plt.legend(handles, labels, loc="upper right")

        fig.savefig(outdir / f"{prefix}_{kind}_{loss}_{n}.pdf", bbox_inches="tight")


def plot_ultra_pareto_timings(
    rep_timings,
    kernel_timings,
    curves,
    loss="rmse",
    prefix="t_ultra",
    ns=trainsetsizes,
    outdir=Path(),
):
    outdir = Path(outdir)

    losses = {name: curves[name][ns[-1]][loss]["mean"] for name in rep_timings.keys()}

    fig, ax = fig_and_ax(figsize=(9, 6))
    ax.set_xlabel("Time in ms")
    ax.set_ylabel(f"{loss.upper()} of $E_f$ in meV/atom (at $N=10000$)")

    for n in ns:
        # print(f"n={n}\n")
        colors = iter(sns.color_palette("bright", len(rep_timings)))

        handles = []

        for name in rep_timings.keys():

            c = next(colors)
            tr = 1000 * rep_timings[name][n]["run"]["mean"] / 1000.0
            tk = 1000 * kernel_timings[name][n]["mean"] / 1000.0
            l = losses[name]
            ax.scatter(
                tk + tr, l, color=c, marker="o", label=names.get(name, name), alpha=0.3
            )
            # ax.scatter(
            #     tr, l, color=c, marker="$R$", label=names.get(name, name), alpha=0.3
            # )
            # ax.scatter(
            #     tk, l, color=c, marker="$K$", label=names.get(name, name), alpha=0.3
            # )

            # handles.append((p1, p2, p3))

            # print(f"{name}: {t:.2f} @ {l:.2f}")

    handles, labels = plt.gca().get_legend_handles_labels()
    labels, ids = np.unique(labels, return_index=True)
    handles = [handles[i] for i in ids]
    plt.legend(handles, labels, loc="upper right")

    fig.savefig(outdir / f"{prefix}_{loss}.pdf", bbox_inches="tight")
