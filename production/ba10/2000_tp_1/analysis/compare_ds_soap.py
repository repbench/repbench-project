from helpers import *
from plotting import plot_curves
from tabling import print_curves

# compare full indexing mbtr
categories = [
    # "m2_pa",
    # "m2fl_pa",
    # "m23fl_pa",
    # "m23_pa",
    # "sf2_ps",
    # "sf23_ps",
    "ds_soap_gto_ps",
    "ds_soap_poly_ps",
    "soap_ps",
]

all_curves = {}
all_selected = {}
for category in categories:
    best, selected = get_best_curve([category + "_a", category + "_b", category + "_c"])

    all_curves[category] = best
    all_selected[category] = selected

print_curves(all_curves, outdir="compare_ds_soap")
plot_curves(all_curves, outdir="compare_ds_soap")
print_curves(all_curves, outdir="compare_ds_soap", loss="mae")
plot_curves(all_curves, outdir="compare_ds_soap", loss="mae")
print("Which were selected?")
for name, selected in all_selected.items():
    stripped = [s.split("_")[-1] for s in selected]
    print(f"{name}: {stripped}")
