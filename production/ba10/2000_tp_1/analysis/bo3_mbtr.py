from helpers import *
from plotting import plot_curves
from tabling import print_curves

# only MBTR variants
categories = [
    "m2_ps",
    "m2_pa",
    "m2nn_ps",
    "m2nn_pa",
    "m23_ps",
    "m23_pa",
    "m23nn_ps",
    "m23nn_pa",
    "m23mini_ps",
    "m23mini_pa",
    "m23fn_ps",
    "m23fn_pa",
    "mbtr2_pa",
    "mbtr2_ps",
    "mbtr23_pa",
    "mbtr23_ps",
]

all_curves = {}
all_selected = {}
for category in categories:
    best, selected = get_best_curve([category + "_a", category + "_b", category + "_c"])

    all_curves[category] = best
    all_selected[category] = selected

print_curves(all_curves, outdir="bo3_mbtr")
plot_curves(all_curves, outdir="bo3_mbtr")
print_curves(all_curves, outdir="bo3_mbtr", loss="mae")
plot_curves(all_curves, outdir="bo3_mbtr", loss="mae")
print("Which were selected?")
for name, selected in all_selected.items():
    stripped = [s.split("_")[-1] for s in selected]
    print(f"{name}: {stripped}")
