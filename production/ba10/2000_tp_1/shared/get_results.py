from cmlkit.tune import Run
from cmlkit.engine.inout import read_yaml

from .config import trainsetsizes


def get_model(curve, n):
    if (curve / f"run_{n}" / "refined_suggestion-0.yml").is_file():
        model = read_yaml(curve / f"run_{n}" / "refined_suggestion-0.yml")
        return model
    else:
        raise ValueError(f"Could not find model for {curve}/run_{n}")


def get_models(curve):
    return [get_model(curve, n) for n in trainsetsizes]
