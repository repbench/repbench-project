import pathlib
import shutil

for space in pathlib.Path("../spaces").glob("*.yml"):
    name = space.stem

    for suffix in ["_a", "_b", "_c"]:
        folder = pathlib.Path(name + suffix)
        folder.mkdir()
        shutil.copy(space, folder / "space.yml")
