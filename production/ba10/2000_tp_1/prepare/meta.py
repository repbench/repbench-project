import pathlib
import shutil

for prefix in [
    "sf2_",
    "sf23_",
    "m2_",
    "m23_",
    "m2nn_",
    "m23nn_",
    "ds_soap_poly_",
    "ds_soap_gto_",
]:
    for folder in pathlib.Path().glob(f"../{prefix}*"):
        # if ".yml" not in str(folder):  # since the yaml files also match
        shutil.copy(f"{prefix}meta.yml", folder / "meta.yml")
