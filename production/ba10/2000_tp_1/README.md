## Production run for BA10 with 2000 optimisation steps.

Full-scale production run for BA10.

Due to (currently undiagnosed) problems with `quippy` SOAP, we are using `dscribe` for the `ba10` dataset.

## Details

- I am submitting all runs to `talos`.
- Everything is run with `cmlkit` version `2.0.0-alpha14`. (The version is tagged on Github.)
- `qmmlpack` is on the `development` branch, commit `e47ad2e82bdd5f3638dadf0e7ab1368435515d5e`.
- `runner` is on version `RuNNer-v1_00---2019-02-04`, compiled with the stock makefile, commit `355b60a51ff095972dbc1406ce645979f36e40cf`.

The `poetry.lock` file for `talos` is in this folder.

***

Later, after a bugfix, `quippy` SOAP was added. The draco lockfile for these runs is also in this folder. 

- `quippy` is on the `public` branch, commit `0be15664c5d508d8f1c55e3ec5a9502586762a0d`.

The same draco lockfile also describes the environment used for the "new-style" timing runs.

***

Later, adding "full enumeration" mbtr runs, with the `talos2.lock` file.

Same lockfile for predictions.

## Release note

To keep repository size down, only the finally used MBTR runs are included.
