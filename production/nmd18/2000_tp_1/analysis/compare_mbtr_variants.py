from helpers import *
from plotting import plot_curves
from tabling import print_curves

# Generate PRIMARY results: learning curves, timings
# in a readable format.
# also does some plotting, but that's mainly for
# diagnostic purposes, publication plotting is separate.

categories = [
    "m2_ps",
    "m2_pa",
    "m2_pn",
    "m23_ps",
    "m23_pa",
    "m23_pn",
    # "m2fl_pa",
    # "m23fl_pa",
    "mbtr2_ps",
    "mbtr2_pa",
    "mbtr23_ps",
    "mbtr23_pa",
]

all_curves = {}
all_selected = {}
for category in categories:
    best, selected = get_best_curve([category + "_a", category + "_b", category + "_c"])

    all_curves[category] = best
    all_selected[category] = selected

plot_curves(all_curves, outdir="compare_mbtr_variants")
plot_curves(all_curves, outdir="compare_mbtr_variants", loss="mae")
plot_curves(all_curves, outdir="compare_mbtr_variants", loss="rmsle")

print_curves(all_curves, outdir="compare_mbtr_variants")
print_curves(all_curves, outdir="compare_mbtr_variants", loss="mae")
print_curves(all_curves, outdir="compare_mbtr_variants", loss="rmsle")


# also save a human-readable version of "which restarts were picked?"
nice_selected = {}
for name, selected in all_selected.items():
    stripped = [s.split("_")[-1] for s in selected]
    nice_selected[name] = stripped

cmlkit.save_yaml("compare_mbtr_variants/selected.yml", nice_selected)
