Choice of pn/pa/ps makes no difference. Will use ps for consistency with other methods.

See also the "bo3_mbtr" folder for a similar discussion, but with further variants.