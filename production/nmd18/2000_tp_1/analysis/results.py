from helpers import *
from plotting import plot_curves
from tabling import print_curves

# Generate PRIMARY results: learning curves, timings
# in a readable format.
# also does some plotting, but that's mainly for
# diagnostic purposes, publication plotting is separate.

categories = [
    "m2_pa",
    "sf2_ps",
    "sf23_ps",
    "soap_ps",
    "ds_soap_gto_ps",
    "ds_soap_poly_ps",
    "mbtr2_ps",
    "mbtr2_pa",
    "mbtr23_ps",
    "mbtr23_pa",
]

all_curves = {}
all_selected = {}
all_rep_timings = {}
all_rep_cpus = {}
all_kernel_timings = {}
all_kernel_cpus = {}
all_models = {}
for category in categories:
    best, selected = get_best_curve([category + "_a", category + "_b", category + "_c"])

    all_curves[category] = best
    all_selected[category] = selected

    rep_timings, _, rep_cpu = get_best_timings(
        [category + "_a", category + "_b", category + "_c"], selected=selected, kind="rep"
    )
    all_rep_timings[category] = rep_timings
    all_rep_cpus[category] = rep_cpu

    kernel_timings, _, kernel_cpu = get_best_timings(
        [category + "_a", category + "_b", category + "_c"],
        selected=selected,
        kind="kernel",
    )
    all_kernel_timings[category] = kernel_timings
    all_kernel_cpus[category] = kernel_cpu

    all_models[category] = {
        n: get_model(selected[i], n) for i, n in enumerate(trainsetsizes)
    }

plot_curves(all_curves, outdir="results")
plot_curves(all_curves, outdir="results", loss="mae")

cmlkit.save_yaml("results/rep_timings.yml", all_rep_timings)
cmlkit.save_yaml("results/kernel_timings.yml", all_kernel_timings)
cmlkit.save_yaml("results/rep_timings_cpus.yml", all_rep_cpus)
cmlkit.save_yaml("results/kernel_timings_cpus.yml", all_kernel_cpus)

# now load additional stuff that doesn't have timings yet but needs to be included regardless

additional = {
    # the 'fl' runs all got cancelled
    # "m2fl_pa",
    # "m23fl_pa",
    # "m2fl_ps",
    # "m23fl_ps",
    "m23_pa",
    "m2_ps",
    "m23_ps",
    "m2_pn",
    "m23_pn",
}

for category in additional:
    best, selected = get_best_curve([category + "_a", category + "_b", category + "_c"])

    all_curves[category] = best
    all_selected[category] = selected

    all_models[category] = {
        n: get_model(selected[i], n) for i, n in enumerate(trainsetsizes)
    }

print_curves(all_curves, outdir="results")
print_curves(all_curves, outdir="results", loss="mae")

cmlkit.save_yaml("results/curves.yml", all_curves)
for category, models in all_models.items():
    cmlkit.save_yaml(f"results/models/{category}.yml", models)


# also save a human-readable version of "which restarts were picked?"
nice_selected = {}
for name, selected in all_selected.items():
    stripped = [s.split("_")[-1] for s in selected]
    nice_selected[name] = stripped

cmlkit.save_yaml("results/selected.yml", nice_selected)
