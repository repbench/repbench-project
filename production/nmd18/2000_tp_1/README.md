# Full scale NMD18 run

No expense spared!

## Details

- I am submitting all runs to `talos`, except `quippy` SOAP, which runs on `draco`.
- Everything is run with `cmlkit` version `2.0.0-alpha16`. (The version is tagged on Github.)
- `qmmlpack` is on the `development` branch, commit `e47ad2e82bdd5f3638dadf0e7ab1368435515d5e`.
- `runner` is on version `RuNNer-v1_00---2019-02-04`, compiled with the stock makefile, commit `355b60a51ff095972dbc1406ce645979f36e40cf`.

The `poetry.lock` file for `talos` is in this folder. 

The second lock file is the environment used for the following post-procesing tasks: `curve`, `bonus nomad`, as well as one timing run. Reasons for the updgrade include the addition of `rmsle`, and inclusion of `cpuinfo` in timing runs.

***

There is also a lock file for `draco`. This environment was used for the `soap` runs.

- `quippy` is on the `public` branch, commit `0be15664c5d508d8f1c55e3ec5a9502586762a0d`.

The same environment was also used for new timings.

***

`talos3`/`draco2` for revamped MBTR.