import pathlib
import shutil

for prefix in ["sf2", "sf23", "m2", "m23", "ds_soap_poly", "ds_soap_gto"]:
    if prefix == "m2" or prefix == "m23":
        suffixes = ["_pa", "_ps", "_pn"]
    else:
        suffixes = ["_ps"]

    for suffix in suffixes:
        for repeat in ["_a", "_b", "_c"]:
            name = prefix + suffix
            # print(name)
            folder = pathlib.Path("../" + name + repeat)
            folder.mkdir(exist_ok=True)
            shutil.copy("../../spaces/" + name + ".yml", folder / "space.yml")
            shutil.copy("meta_" + prefix + ".yml", folder / "meta.yml")
