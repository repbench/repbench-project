# Production runs for repbench paper

This folder contains the "production" runs, which should not be used for development of the infrastructure and/or experiments.

Therefore, the following rules apply:

- All runs must be continued until the `maxevals` is reached (no unconverged results unless literally unavoidable)
- Only once all runs are finished are curves + timings generated

The folder structure is as follows:

- `ba10/*` all curvesets using the `ba10` dataset
- `gdb9/*` all curvesets using the `gdb9` dataset
- `nmd18/*` all curvesets using the `nmd18` dataset
- `xnmd18/*` all curvesets using the `xnmd18` dataset

Inside of each, there are folders

- `CURVESETS/` which are individual folders collecting every run needed to produce one particular set of learning curves. The naming scheme here is `MAXEVALS_METHOD[_MODIFIER]`. The only method tried is `TPE` for Tree-Structured Parzen Estimators.

Inside each curveset folder, there are

- `shared/` which contains the boilerplate defining how the runner is instantiated, what dataset to use, etc.
- `analysis/` which contains post-processing and individual investigations
- `RUNS/` which are folders containing ONE particular set of optimisation runs for ONE particular model class. The naming scheme is `SPACENAME[_RERUN]`. Reruns are labelled `a`, `b`, `c`.

Inside of each run there are exactly two files: `meta.yml` which defines how long we expect a run to take, how many workers to use, etc., and a copied `space.yml`.

As much as possible, every folder should contain a `README.md` explaining what is going on.

***

The heuristics for choosing parametrisations are as follows:

(Further considerations can be found inside the individual `yml` files.)

# Cutoffs for local descriptors
- Integer choices.
- Minimum is the minimum distance in the dataset rounded up (`+1` for `gdb9` since it 1A is very small).
- Maximum is 10 (after discussion with Jörg Behler), or 9 in cases where 10 leads to problems (should not make a difference, chosen models favour smaller cutoffs anyway).

# Broadening parameters
- Chosen on a log2grid with `0.5` spacing for the exponent. 
- Default from `-20` to `+20`, but maximum may be constrained to something closer to the range of the quantity that's being broadened, mostly we chose around `log2(max) + 3`. (See below for MBTR heuristics, which are slightly different.)

# Other parameters
- On a log2grid with `1.0` spacing for the exponent if continuous/if a log grid makes sense.
- Choice between integers/options otheriwse.

# Original MBTR
- Discretisation bins for `MBTR` are held constant at `100`. (Preliminary tests showed no strong influence of this value, as long as it's not too small.)
- `k=2,3` terms are always computed without double-counting for performance.
- `k=3` has reduced accuracy parameter for performance.

# "Reparametrised" MBTR

We noticed some inconsistencies with the original MBTR parametrisations, so here is a refined version.

- `num` = 100, always. This is simply *by fiat*.
- `acc` = 0.001, always. This is also *by fiat*.
- `full` indexing, always. This avoids unclear scaling effects.
- Ranges for `geomfs` which are used as follows (md is the min distance in the whole dataset):
	- "1/distance": [-0.05 / md, 1.05 / md]
	- "1/dot": [-0.05 / (md ** 2), 1.05 / (md ** 2)]
	- "angle": [-0.05 * pi, 1.05 * pi]
	- "cos_angle": [-1.05, 1.05]
	- "dot/dotdot": [-1.05 / (md ** 2), 1.05 / (md ** 2)]
- Broadenings (if W is width of geomf interval): [W/(2 x n), W/8], i.e. so that 2 sigma is larger than one bin and smaller than 1/4 the interval.
- Length scales for weighting functions from 2 ** 0 = 1 to a max value chosen empirically so that total timings remain below approx. 5-10 seconds, to avoid costly timetous. Here we use 0.5 spacing if the max exponent is <= 2 to avoid having an overly coarse grid. Here are the max exponents used:
	- `xnmd18` (and also for consistency `nmd18`): 
		- `1/distance`:
			- "exp_-1/identity": 3.0
			- "exp_-1/identity^2": 9.0
		- `1/dot`:
			- "exp_-1/identity": 5.0
			- "exp_-1/identity^2": 15.0
		- `angle`:
			- `exp_-1/normnormnorm`: 1.5
			- `exp_-1/norm+norm+norm`: 1.0
		- `cos_angle`:
			- `exp_-1/normnormnorm`: 1.5
			- `exp_-1/norm+norm+norm`: 1.0
		- `cos_angle`:
			- `exp_-1/normnormnorm`: 2.0
			- `exp_-1/norm+norm+norm`: 1.0


- We use the `simple` norm, with scaling 1.0, or no norm at all.

# SF
- For radial SFs, we use both common parametrisation schemes as written up in the Gastegger et al. paper (see `cmlkit` source for reference.) It is not feasible to parametrise individual SFs with the current infrastructure.
- For angular SFs, we use: `zeta=1,2,4,16` and `lambda +/- 1`, `eta` on log2grid `[-20, 1]` (it's something like a broadening for the purposes of the above heuristics) per discussion with Jörg Behler. (He recommended `eta=0` for small cutoffs, but this is not possible to implement at the moment).
- We do not parametrise for individual element combinations separately, since KRR with the Gaussian kernel is insensitive to entries in the feature vector that have constant values across the dataset (as opposed to NNs), so we can afford to have "overcomplete" parametrisations.

# SOAP
- For perfomance reasons, cap `n` and `l` at 8.
- For `ba10`, we might have to concatenate multiple SOAPs -- this is under investigation.

# KRR
- Always same settings: The stochastic optimiser picks starting parameters on a log2grid with stepsize 1.0, then a local grid search refines on a log2grid with 0.5 spacing.
