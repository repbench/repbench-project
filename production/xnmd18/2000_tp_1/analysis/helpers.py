import cmlkit

import numpy as np
from pathlib import Path
import sys

sys.path.append("..")

from shared import trainsetsizes

curveset = Path(__file__).parent / ".."


def load_curve(name):
    return cmlkit.read_yaml(curveset / f"{name}/post/curve.yml")


def load_nomad(name):
    return cmlkit.read_yaml(curveset / f"{name}/post/nomad/results.yml")


def raw_curve(curve, loss="rmse"):
    return np.array([entry[loss]["mean"] for entry in curve.values()])


def get_model(name, n):
    return cmlkit.read_yaml(curveset / f"{name}/run_{n}/refined_suggestion-0.yml")


def load_timings(name, kind="rep", cpu="6-79"):
    return cmlkit.read_yaml(curveset / f"{name}/post/time/{kind}/{cpu}/times.yml")


def load_cpu(name, kind="rep", cpu="6-79"):
    return cmlkit.read_yaml(curveset / f"{name}/post/time/{kind}/{cpu}/cpu.yml")


def get_opt_losses(candidates):
    losses = [
        np.array(
            [
                open(curveset / c / f"run_{n}/status.txt", "r")
                .readlines()[3]
                .split(" ")[3]
                for c in candidates
            ],
            dtype=float,
        )
        for n in trainsetsizes
    ]
    return losses


def get_best_curve(candidates):
    """Get best learning curve, by optimised loss"""

    losses = get_opt_losses(candidates)

    lc = {}
    best_is = []
    for i, n in enumerate(trainsetsizes):
        best_i = np.argmin(losses[i])
        best_is.append(best_i)
        lc[n] = load_curve(candidates[best_i])[n]

    return lc, [candidates[i] for i in best_is]


def get_best_timings(candidates, selected=None, kind="rep", cpu="6-79"):
    """Get best timings"""

    if selected is None:
        _, selected = get_best_curve(candidates)

    all_timings = {c: load_timings(c, kind=kind, cpu=cpu) for c in candidates}
    all_cpu = {c: load_cpu(c, kind=kind, cpu=cpu) for c in candidates}

    timings = {}
    cpu = {}
    for i, n in enumerate(trainsetsizes):
        timings[n] = all_timings[selected[i]][n]
        cpu[n] = all_cpu[selected[i]]

    return timings, selected, cpu
