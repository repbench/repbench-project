from helpers import *
from plotting import plot_curves, plot_pareto_timings
from tabling import print_curves

# Plot selected curves.
categories = [
    "sf2_ps",
    "sf23_ps",
    "ds_soap_gto_ps",
    "ds_soap_poly_ps",
    # "m2_ps",
    # "m2_pn",
    "m2_pa",
    # "m23_ps",
    # "m23_pn",
    "m23_pa",
]


for category in categories:
    print(f"\n{category}:")
    _, selected = get_best_curve([category + "_a", category + "_b", category + "_c"])

    these_results = []
    for i, s in enumerate(selected):
        nomad = load_nomad(s)
        values = nomad[trainsetsizes[i]]
        print(
            f"RMSLE: {np.round(1000*values['full']['rmsle'], 2)}/{np.round(1000*values['nodup']['rmsle'], 2)}"
        )
        print(
            f"MAE: {np.round(1000*values['full']['mae'], 2)}/{np.round(1000*values['nodup']['mae'], 2)}"
        )
        print()
