from helpers import *


def print_curves(curves, loss="rmse", prefix="lc", outdir=Path()):
    """Print learning curves in tabular format

    Args:
        curves: dict with keys corresponding to names of curve,
            and values a standard learning curve dict
        loss: loss to be used
        prefix: filename will be prefix_lossname.txt
        outdir: where to save plots

    """
    outdir = Path(outdir)
    report = f"\n\n ### Table for loss {loss} (meV/cation; mean (std)) ### \n"

    sizes = f"{loss}! sizes"
    s = f"{sizes:>24}: "
    for n in trainsetsizes:
        s += f"{n:<16}"
    report += s + "\n"

    for name, curve in curves.items():
        mean = [r[loss]["mean"]*1000 for r in curve.values()]
        std = [r[loss]["std"]*1000 for r in curve.values()]

        s = f"{name:>24}: "

        for i in range(len(mean)):
            s += f"{mean[i]:6.2f} ({std[i]:6.2f}) "

        report += s + "\n"

    print(report)

    with open(outdir / f"{prefix}_{loss}.txt", "w+") as f:
        f.write(report)
