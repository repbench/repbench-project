## Full-scale production run for relaxed nomad

## Details

- I am submitting all runs to `talos`. Currently no `quippy` planned.
- `qmmlpack` is on the `development` branch, commit `e47ad2e82bdd5f3638dadf0e7ab1368435515d5e`.
- `runner` is on version `RuNNer-v1_00---2019-02-04`, compiled with the stock makefile, commit `355b60a51ff095972dbc1406ce645979f36e40cf`.

The `poetry.lock` file for `talos` is in this folder. 

***

Later ran `quippy`. `draco` lockfile in folder.

- `quippy` is on the `public` branch, commit `0be15664c5d508d8f1c55e3ec5a9502586762a0d`.

***

`talos2`/`draco2` are the lockfiles for the revamped MBTR runs.