from cmlkit.tune import Hyperopt, Run
from cmlkit.engine.inout import read_yaml

from .config import data, trainsetsizes

tuner = {
    "krr_tuner_cv": {
        "optimizer": {"opt_lgs": {"resolution": None, "rng": 123, "maxevals": 25}}
    }
}


def make_run(space, n):
    evaluator = {
        "eval_skrrt": {"data": data.name_ot(1, n), "target": "u0", "tuner": tuner}
    }

    search = Hyperopt(space=read_yaml(space), method="tpe")

    run = Run(
        search=search,
        evaluator=evaluator,
        stop={"stop_max": {"count": 2000}},
        trial_timeout=10 * float(n),  # avoid timeouts at all costs
        caught_exceptions=["QMMLException", "TimeoutError"],
        name=str(n),
    )

    return run
