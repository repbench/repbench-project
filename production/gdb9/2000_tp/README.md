# `gdb9-2000_tpe` curveset

This is a full production curveset, with at least 2000 evaluations per model. No expense spared!

## Details

- I am submitting all runs to `talos`, except the ones using `soap`, which is running on `draco`.
- Everything is run with `cmlkit` version `2.0.0-alpha12`. (The version is tagged on Github.)
- `qmmlpack` is on the `development` branch, commit `e47ad2e82bdd5f3638dadf0e7ab1368435515d5e`.
- `runner` is on version `RuNNer-v1_00---2019-02-04`, compiled with the stock makefile.
- `quippy` is on the `public` branch, commit `0be15664c5d508d8f1c55e3ec5a9502586762a0d`.

The `poetry.lock` files on `draco` and `talos` are in this folder. These environments were used for:

- Optimisation runs
- Curve generation
- "Original" timings

The `draco_2` lockfile was used for:

- "New" timings.
- `co3` soap runs.

The `talos2` lockfile:

- `ds` runs!

The `draco3` lockfile: Reparametrised MBTR runs, SF23r

The `talos3` lockfile: SF23r
