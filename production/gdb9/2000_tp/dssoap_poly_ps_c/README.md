JUST A DIAGNOSTIC RUN; run on `talos` with the environment of `ba10/0100_tp_1`, not the one present for all other runs.

This is mainly to find out whether SOAP divergences are implementation-specific.