from helpers import *
from plotting import plot_curves, plot_combo_pareto_timings, plot_pareto_timings

# Just try the timings
categories = ["m2_ps", "m23_ps", "sf2_ps", "sf23_ps", "soap_ps"]

all_curves = {}
all_selected = {}
all_rep_timings = {}
all_kernel_timings = {}
all_models = {}
for category in categories:
    best, selected = get_best_curve([category + "_a", category + "_b", category + "_c"])

    all_curves[category] = best
    all_selected[category] = selected

    rep_timings, _ = get_best_timings(
        [category + "_a", category + "_b", category + "_c"], selected=selected, kind="rep"
    )
    all_rep_timings[category] = rep_timings

    kernel_timings, _ = get_best_timings(
        [category + "_a", category + "_b", category + "_c"], selected=selected, kind="kernel"
    )
    all_kernel_timings[category] = kernel_timings

    all_models[category] = {
        n: get_model(selected[i], n) for i, n in enumerate(trainsetsizes)
    }

# print_curves(all_curves, outdir="results")
# plot_curves(all_curves, outdir="results")
plot_combo_pareto_timings(all_rep_timings, all_kernel_timings, all_curves, outdir="timings")
# plot_pareto_timings(all_kernel_timings, all_curves, outdir="timings", kind="kernel")
# plot_ultra_pareto_timings(all_rep_timings, all_kernel_timings, all_curves, outdir="timings")


print("Which were selected?")
for name, selected in all_selected.items():
    stripped = [s.split("_")[-1] for s in selected]
    print(f"{name}: {stripped}")
