from helpers import *
from plotting import plot_curves
from tabling import print_curves

# how well does CV loss predict final loss?
# , "sf23_ps", "soap_ps", "m2_ps", "m23_ps"

categories = ["sf2_ps", "sf23_ps", "soap_ps", "m2_ps", "m23_ps"]

cv_losses = {}
final_losses = {}
all_selected = {}
all_best = {}
for category in categories:
    cv_losses[category] = [
        [get_single_opt_loss(curve, n) for n in trainsetsizes]
        for curve in [category + "_a", category + "_b", category + "_c"]
    ]
    final_losses[category] = [
        [load_curve(curve)[n]["rmse"]["mean"] for n in trainsetsizes]
        for curve in [category + "_a", category + "_b", category + "_c"]
    ]

    best, selected = get_best_curve([category + "_a", category + "_b", category + "_c"])
    all_selected[category] = selected
    all_best[category] = best

print(
    "We now compare the final CV losses (cv) and the final learning curve losses (fi) to see if there are any mismatches."
)
print(
    "We also compare the best final learning curve loss with the one picked via CV. If >0, there is a mismatch and we could have done better."
)

for category in categories:
    print(category)
    for j, c in enumerate([category + "_a", category + "_b", category + "_c"]):
        line = f"{c:>20} (cv) "
        for i in range(6):
            if all_selected[category][i] == c:
                line += f"{cv_losses[category][j][i]:7.3f}* "
            else:
                line += f"{cv_losses[category][j][i]:7.3f}  "

        print(line)

        line = f"{c:>20} (fi) "
        for i in range(6):
            if all_selected[category][i] == c:
                line += f"{final_losses[category][j][i]:7.3f}* "
            else:
                line += f"{final_losses[category][j][i]:7.3f}  "

        print(line)
        print()

    # did the best cv loss also get the best result?
    line = f"{'Match:':>26}"
    for i in range(6):
        best_via_cv = all_best[category][trainsetsizes[i]]["rmse"]["mean"]
        real_best = np.min([final_losses[category][j][i] for j in range(3)])
        line += f"{np.abs(real_best-best_via_cv):7.3f}  "

    print(line)
