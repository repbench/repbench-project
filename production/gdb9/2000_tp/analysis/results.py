from helpers import *
from plotting import (
    plot_curves,
    plot_combo_pareto_timings,
    plot_pareto_timings,
    trainsetsizes,
)
from tabling import print_curves, print_timings

# this generates the "main results", i.e. learning curves and timings in multiple formats.
# also saves out the finally selected models.
# everything is best of 3 identical reruns.

categories = [
    "m2_ps",
    "m23_ps",
    "sf2_ps",
    "sf23_ps",
    "sf23r_ps",
    "soap_ps",
    "ds_soap_gto_ps",
    "dssoap_poly_ps",
    "mbtr2_pa",
    "mbtr23_pa",
    "mbtr2_ps",
    "mbtr23_ps",
]

all_curves = {}
all_selected = {}
all_rep_timings = {}
all_rep_cpus = {}
all_kernel_timings = {}
all_kernel_cpus = {}
all_models = {}
for category in categories:
    best, selected = get_best_curve([category + "_a", category + "_b", category + "_c"])

    all_curves[category] = best
    all_selected[category] = selected

    rep_timings, _, rep_cpu = get_best_timings(
        [category + "_a", category + "_b", category + "_c"], selected=selected, kind="rep"
    )
    all_rep_timings[category] = rep_timings
    all_rep_cpus[category] = rep_cpu

    kernel_timings, _, kernel_cpu = get_best_timings(
        [category + "_a", category + "_b", category + "_c"],
        selected=selected,
        kind="kernel",
    )
    all_kernel_timings[category] = kernel_timings
    all_kernel_cpus[category] = kernel_cpu

    all_models[category] = {
        n: get_model(selected[i], n) for i, n in enumerate(trainsetsizes)
    }

plot_curves(all_curves, outdir="results", loss="rmse")
plot_curves(all_curves, outdir="results", loss="mae")

# not plotted, but still important
additional = [
    "soap_co3_ps",
    "ds_soap_co3_gto_ps",
    "ds_soap_co3_poly_ps",
    "m2fl_ps",
    "m23fl_ps",
    "m2fl_pa",
    "m23fl_pa",
    "m2_pa",
    "m23_pa",
    "m2nfl_ps",
    "m23nfl_ps",
    "m2nfl_pa",
    "m23nfl_pa",
]

for category in additional:
    best, selected = get_best_curve([category + "_a", category + "_b", category + "_c"])

    all_curves[category] = best

    all_selected[category] = selected
    all_models[category] = {
        n: get_model(selected[i], n) for i, n in enumerate(trainsetsizes)
    }


print_curves(all_curves, outdir="results", loss="rmse")
print_curves(all_curves, outdir="results", loss="mae")

cmlkit.save_yaml("results/rep_timings.yml", all_rep_timings)
cmlkit.save_yaml("results/kernel_timings.yml", all_kernel_timings)
cmlkit.save_yaml("results/rep_timings_cpus.yml", all_rep_cpus)
cmlkit.save_yaml("results/kernel_timings_cpus.yml", all_kernel_cpus)

cmlkit.save_yaml("results/curves.yml", all_curves)
for category, models in all_models.items():
    cmlkit.save_yaml(f"results/models/{category}.yml", models)

# also save a human-readable version of "which restarts were picked?"
nice_selected = {}
for name, selected in all_selected.items():
    stripped = [s.split("_")[-1] for s in selected]
    nice_selected[name] = stripped

cmlkit.save_yaml("results/selected.yml", nice_selected)
