from helpers import *
from plotting import plot_curves
from tabling import print_curves

# in this, compare these "best of 2" with the 1000 sample "best of 2"
# TODO: include SOAP (running b for this + 1000 ones are in the queue)

categories = [
    "m12_ps",
    "m123_ps",
    "sf2_ps",
    "sf23_ps",
    "../1000_tp/m12_ps",
    "../1000_tp/m123_ps",
    "../1000_tp/sf2_ps",
    "../1000_tp/sf23_ps",
]

all_curves = {}
all_selected = {}
for category in categories:
    best, selected = get_best_curve([category + "_a", category + "_b"])

    all_curves[category] = best
    all_selected[category] = selected

print_curves(all_curves, outdir="compare_with_1000")
plot_curves(all_curves, outdir="compare_with_1000")
