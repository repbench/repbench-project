import cmlkit
import numpy as np
from pathlib import Path

print("pa / ps / pa-ps")

print("## SF2")

sf2_pa = np.array(
    [
        result["rmse"]["mean"]
        for result in cmlkit.read_yaml(Path("../sf2_pa") / "post/curve.yml").values()
    ]
)
sf2_ps = np.array(
    [
        result["rmse"]["mean"]
        for result in cmlkit.read_yaml(Path("../sf2_ps") / "post/curve.yml").values()
    ]
)

print(sf2_pa)
print(sf2_ps)
print(sf2_pa - sf2_ps)


print("## SF23")

sf23_pa = np.array(
    [
        result["rmse"]["mean"]
        for result in cmlkit.read_yaml(Path("../sf23_pa") / "post/curve.yml").values()
    ]
)
sf23_ps = np.array(
    [
        result["rmse"]["mean"]
        for result in cmlkit.read_yaml(Path("../sf23_ps") / "post/curve.yml").values()
    ]
)

print(sf23_pa)
print(sf23_ps)
print(sf23_pa - sf23_ps)


print("## MBTR12")

m12_pa = np.array(
    [
        result["rmse"]["mean"]
        for result in cmlkit.read_yaml(Path("../m12_pa") / "post/curve.yml").values()
    ]
)
m12_ps = np.array(
    [
        result["rmse"]["mean"]
        for result in cmlkit.read_yaml(Path("../m12_ps") / "post/curve.yml").values()
    ]
)

print(m12_pa)
print(m12_ps)
print(m12_pa - m12_ps)


print("## MBTR123")

m123_pa = np.array(
    [
        result["rmse"]["mean"]
        for result in cmlkit.read_yaml(Path("../m123_pa") / "post/curve.yml").values()
    ]
)
m123_ps = np.array(
    [
        result["rmse"]["mean"]
        for result in cmlkit.read_yaml(Path("../m123_ps") / "post/curve.yml").values()
    ]
)

print(m123_pa)
print(m123_ps)
print(m123_pa - m123_ps)


print("## SOAP")

soap_pa = np.array(
    [
        result["rmse"]["mean"]
        for result in cmlkit.read_yaml(Path("../soap_pa") / "post/curve.yml").values()
    ]
)
soap_ps = np.array(
    [
        result["rmse"]["mean"]
        for result in cmlkit.read_yaml(Path("../soap_ps") / "post/curve.yml").values()
    ]
)

print(soap_pa)
print(soap_ps)
print(soap_pa - soap_ps)
