from helpers import *
from plotting import plot_curves
from tabling import print_curves

# in this, compare sf23 reruns

curves = {c: load_curve(c) for c in ["sf23_ps_a", "sf23_ps_b", "sf23_ps_c"]}
print("Reminder: sf23_ps: ['c', 'c', 'a', 'a', 'a', 'a']")
print_curves(curves, outdir="compare_sf23_reruns")
plot_curves(curves, outdir="compare_sf23_reruns")
