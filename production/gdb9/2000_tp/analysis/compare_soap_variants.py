from helpers import *
from plotting import (
    plot_curves,
    plot_combo_pareto_timings,
    plot_pareto_timings,
    trainsetsizes,
)
from tabling import print_curves, print_timings

# compare various soap variants

categories = [
    "soap_ps",
    "ds_soap_gto_ps",
    "dssoap_poly_ps",
    "soap_co3_ps",
    "ds_soap_co3_gto_ps",
    # "ds_soap_co3_poly_ps", # missing
]

all_curves = {}
all_selected = {}
all_models = {}
for category in categories:
    best, selected = get_best_curve([category + "_a", category + "_b", category + "_c"])
    all_curves[category] = best

    all_selected[category] = selected
    all_models[category] = {
        n: get_model(selected[i], n) for i, n in enumerate(trainsetsizes)
    }

print_curves(all_curves, outdir="compare_soap_variants", loss="rmse")
plot_curves(all_curves, outdir="compare_soap_variants", loss="rmse")

print_curves(all_curves, outdir="compare_soap_variants", loss="mae")
plot_curves(all_curves, outdir="compare_soap_variants", loss="mae")
