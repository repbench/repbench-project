This folder contains various scripts analysing aspects of the data collected.

The most relevant part of it is the `results` sub-folder and the `results.py` script, which generates the "final results" for the paper. 

Everything else may or may not be interesting, not all of it concerns model classes that are used for the final results, but documents analyses that were made to guide the research in the direction it has taken.

## Note for release

Some things that were deemed of only minor interest have been removed. Note that the scripts include references to model classes that were ommitted in this repository, including many, many MBTR variations.