from helpers import *
from plotting import plot_curves
from tabling import print_curves

# in this, we check how identical reruns affect the results,
# i.e. when did we just get "lucky" vs "unlucky"

categories = ["sf2_ps", "sf23_ps", "soap_ps", "m2_ps", "m23_ps"]

## best of 2 ##

all_curves = {}
all_selected = {}
for category in categories:
    best, selected = get_best_curve([category + "_a", category + "_b"])

    all_curves[category] = best
    all_selected[category] = selected

print_curves(all_curves, outdir="inspect_reruns", prefix="bo2")
plot_curves(all_curves, outdir="inspect_reruns", prefix="bo2")
print("Which were selected?")
for name, selected in all_selected.items():
    stripped = [s.split("_")[2] for s in selected]
    print(f"{name}: {stripped}")

## best of 3 ##

all_curves = {}
all_selected = {}
for category in categories:
    if category not in ["m123nn_ps"]:
        best, selected = get_best_curve([category + "_a", category + "_b", category + "_c"])
    else:
        best, selected = get_best_curve(
            [category + "_a", category + "_b"]
        )

    all_curves[category] = best
    all_selected[category] = selected

print_curves(all_curves, outdir="inspect_reruns")
plot_curves(all_curves, outdir="inspect_reruns")
print_curves(all_curves, outdir="inspect_reruns", loss="mae")
plot_curves(all_curves, outdir="inspect_reruns", loss="mae")
print("Which were selected?")
for name, selected in all_selected.items():
    stripped = [s.split("_")[2] for s in selected]
    print(f"{name}: {stripped}")


print("\nLet's compare with just option a")
curves = {category + "_a": load_curve(category + "_a") for category in categories}
print_curves(curves, outdir="inspect_reruns", prefix="a")
plot_curves(curves, outdir="inspect_reruns", prefix="a")

print("\nLet's compare with just option b")
curves = {category + "_b": load_curve(category + "_b") for category in categories}
print_curves(curves, outdir="inspect_reruns", prefix="b")
plot_curves(curves, outdir="inspect_reruns", prefix="b")
