from helpers import *
from plotting import plot_curves
from tabling import print_curves

# in this, compare soap runs

curves = {c: load_curve(c) for c in ["soap_ps_a", "soap_ps_b", "soap_ps_c"]}
print("Reminder: Selected:  ['c', 'b', 'a', 'b', 'a', 'c']")
print_curves(curves, outdir="compare_soap_reruns")
plot_curves(curves, outdir="compare_soap_reruns")
