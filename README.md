# `repbench-project`

Overview of [`repbench`](https://marcel.science/repbench/) repositories:

- [`repbench-results`](https://gitlab.com/repbench/repbench-results): results as given in the publication (learning curves, timings, optimised models, final search spaces)
- [`repbench-datasets`](https://gitlab.com/repbench/repbench-datasets): datasets and splits
- [`repbench-project`](https://gitlab.com/repbench/repbench-project) (this): raw working repository

The project relies on the [`cmlkit`](https://github.com/sirmarcel/cmlkit/) package.

***

This repository is a (slightly cleaned-up) copy of my internal working repository for the [`repbench`](https://marcel.science/repbench) project ([preprint](https://arxiv.org/abs/2003.12081)), containing project-specific infrastructure, as well as the raw inputs and results for the reported experiments. It is intended to provide a full picture of how the computational results were obtained, for the purpose of transparency and reproducibility. I do not recommend using it as-is for further projects, and no guarantee is given that the code will run without modification. (But it should be straightforward to adapt to reproduce any given result.)

If you plan to build on this work, please contact `hello@marcel.science`, we may be able to put together a more minimal working example! For small questions, you can also find me on twitter [`@marceldotsci`](https://twitter.com/marceldotsci). In the interest of helping future work, occasional asides are included in readmes pointing out things that could be improved.

## Structure and Architecture

### Folders

- `falco/`: `falco` is the command line utility used for running experiments
- `repbench/`: `cmlkit` components custom to this project
- `production/`: runs used for the paper (and more!)
- `experiments/`: other things
- `data/`: should be a copy of [`repbench-datasets`](https://gitlab.com/repbench/repbench-datasets), omitted here

Folders usually contain additional `README.md` files explaining what's going on.

### Architecture

This project heavily relies on `cmlkit` to provide models, hyper-parameter tuning, and the general infrastructure to describe objects as dictionaries. It makes use of the [`skrrt`](https://gitlab.com/sirmarcel/skrrt), [`mortimer`](https://gitlab.com/sirmarcel/mortimer) and [`cscribe`](https://github.com/sirmarcel/cscribe) plugins. Computations are managed using a small custom command line interface (`falco`), which interfaces with `slurm`.

The core results we generate are learning curves, which we take to be sets of train/validate/test splits at different training set sizes. For each training set size, we optimise a model with a given representation and a KRR regressor, and then predict the test set. The overall "search space" of models with a given representation is called a "model class". We call the set of training cuves for a given dataset and different model classes a "curveset". For the finally chosen model, we also compute run times for both kernel matrix computations and representation generation.

From an infrastructure perspective, each curveset is a folder containing a `shared/` subfolder that is a small `python` package providing a description of the exact settings needed for this experiment, like the number of HP optimisation steps, the number of different training set sizes, and so on. (Aside: With a few additional years of experience, I would recommend not following this approach; instead, the setup should be in a non-code format, and there should be a corresponding class in the project-wide infrastructure.)

Individual representations, for example MBTR with `k=2,3` terms is given a subfolder, for instance `mbtr23`, containing the [`cmlkit.tune`](https://github.com/sirmarcel/cmlkit/tree/master/cmlkit/tune) description of the optimisation space (`space.yaml`). Subfolders then correspond to different training set sizes, and are simply `cmlkit.tune.Run` snapshots. A further subfolder `post/` contains the results of predictions for the test set, and timings. For further details, see the readme of `production/`.

`falco` then provides the necessary infrastructure to simply run `falco prepare mbtr23`, generating the runs for different training set sizes, and then `falco run mbtr23 all` to submit to `slurm`.

#### HP Optimisation Pipeline

We use a two-step approach to HP optimisation: We use the `cmlkit.tune` wrapper for `hyperopt` to stochastically optimise the parameters of the representation, and *initial* parameters for KRR. This approach is trivial to parallelise, as `hyperopt` trials can be obtained and finished in arbitrary order. Each such suggestion is *refined* using `skrrt`, which performs a grid search for the KRR parameters. The final result of this refinement step is reported as loss to the overall `hyperopt` optimisation. The whole process is repeated three times, the best loss is taken as final result. (Note that it appears that the small timing differences in evaluation is sufficient to provide randomness for these restarts.)

(Aside: The TPE optimisation in `hyperopt` is not particularly optimised for this usecase. It is tuned for a far lower number of samples, whereas we use 2000 here. It also requires a few restarts to find a good solution.)

We therefore take the perspective that a learning curve is given with respect to the entire pipeline including HP optimisation, rather than for a single fixed model.

## Setup

This repository is *not* intended for installation, it's largely for reading.

Should you still want to run things, it should work roughly like this:

- Download and setup the `repbench-datasets` repository
- Add the `repbench-datasets` directory to `$CML_DATASET_PATH`
- Add this folder to `$PYTHONPATH` so `repbench` can be imported
- Run `poetry install` (installs `cmlkit` and plugins)
- Add `export CML_PLUGINS=skrrt,mortimer,repbench,cscribe` to your `.bashrc` equivalent
- Add `falco/bin` to `$PATH`
